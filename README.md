<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><p align="center" dir="auto"><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/Stirling-Tools/Stirling-PDF/main/docs/stirling.png"><img src="https://raw.githubusercontent.com/Stirling-Tools/Stirling-PDF/main/docs/stirling.png" width="80" style="max-width: 100%;"></a></p>
<div class="markdown-heading" dir="auto"><h1 align="center" tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">斯特林 PDF</font></font></h1><a id="user-content-stirling-pdf" class="anchor" aria-label="永久链接：Stirling-PDF" href="#stirling-pdf"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><a href="https://hub.docker.com/r/frooodle/s-pdf" rel="nofollow"><img src="https://camo.githubusercontent.com/48562806f8cc9e2908867f4597baf742ed0a9d239a43811e157f2a57aa4d7d60/68747470733a2f2f696d672e736869656c64732e696f2f646f636b65722f70756c6c732f66726f6f6f646c652f732d706466" alt="Docker 拉取" data-canonical-src="https://img.shields.io/docker/pulls/frooodle/s-pdf" style="max-width: 100%;"></a>
<a href="https://discord.gg/Cn8pWhQRxZ" rel="nofollow"><img src="https://camo.githubusercontent.com/12331246a09363ff2363ecb5301f5cffbc878b57c3796db780232282e2ada880/68747470733a2f2f696d672e736869656c64732e696f2f646973636f72642f313036383633363734383831343438333731383f6c6162656c3d446973636f7264" alt="不和谐" data-canonical-src="https://img.shields.io/discord/1068636748814483718?label=Discord" style="max-width: 100%;"></a>
<a href="https://github.com/Stirling-Tools/Stirling-PDF/"><img src="https://camo.githubusercontent.com/b8b36687091a8fd105503594ac783dfd35180f747f524b3ced93eb9e5266aceb/68747470733a2f2f696d672e736869656c64732e696f2f646f636b65722f762f66726f6f6f646c652f732d7064662f6c6174657374" alt="Docker 镜像版本（标签为最新 semver）" data-canonical-src="https://img.shields.io/docker/v/frooodle/s-pdf/latest" style="max-width: 100%;"></a>
<a href="https://github.com/Stirling-Tools/stirling-pdf"><img src="https://camo.githubusercontent.com/04fd48791563d631b885204a785c825cb33a46af971301f2e504f8cab7468a83/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f73746172732f737469726c696e672d746f6f6c732f737469726c696e672d7064663f7374796c653d736f6369616c" alt="GitHub Repo 星星" data-canonical-src="https://img.shields.io/github/stars/stirling-tools/stirling-pdf?style=social" style="max-width: 100%;"></a>
<a href="https://www.paypal.com/donate/?hosted_button_id=MN7JPG5G6G3JL" rel="nofollow"><img src="https://camo.githubusercontent.com/73ffcfb9a7fab2f24e9a13318a61efe906fe41713405d837036b50aeb13c5a57/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f50617970616c253230446f6e6174652d79656c6c6f773f7374796c653d666c6174266c6f676f3d70617970616c" alt="Paypal 捐款" data-canonical-src="https://img.shields.io/badge/Paypal%20Donate-yellow?style=flat&amp;logo=paypal" style="max-width: 100%;"></a>
<a href="https://github.com/sponsors/Frooodle"><img src="https://camo.githubusercontent.com/a328cb6ab7c75b7e9a14d9634e767f2c00a4e9d5ba5e0548072c30ad6191b16a/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f47697468756225323053706f6e736f722d79656c6c6f773f7374796c653d666c6174266c6f676f3d676974687562" alt="Github 赞助商" data-canonical-src="https://img.shields.io/badge/Github%20Sponsor-yellow?style=flat&amp;logo=github" style="max-width: 100%;"></a></p>
<p dir="auto"><a href="https://cloud.digitalocean.com/apps/new?repo=https://github.com/Stirling-Tools/Stirling-PDF/tree/digitalOcean&amp;refcode=c3210994b1af" rel="nofollow"><img src="https://camo.githubusercontent.com/159b79f1d2eb07b3de258a6abef0fdc09c4bbed5cbfb6f51642c27d5f93a3b6d/68747470733a2f2f7777772e6465706c6f79746f646f2e636f6d2f646f2d62746e2d626c75652e737667" alt="部署至 DO" data-canonical-src="https://www.deploytodo.com/do-btn-blue.svg" style="max-width: 100%;"></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这是一个使用 Docker 的强大的、本地托管的基于 Web 的 PDF 操作工具。它使您能够对 PDF 文件执行各种操作，包括拆分、合并、转换、重新组织、添加图像、旋转、压缩等。这个本地托管的 Web 应用程序已经发展到包含一套全面的功能，可以满足您的所有 PDF 需求。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">斯特林 PDF 不会出于记录保存或跟踪目的发起任何外拨电话。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">所有文件和 PDF 要么专门存在于客户端，要么仅在任务执行期间驻留在服务器内存中，要么临时驻留在仅用于执行任务的文件中。到那时，用户下载的任何文件都将从服务器中删除。</font></font></p>
<p dir="auto"><a target="_blank" rel="noopener noreferrer" href="/Stirling-Tools/Stirling-PDF/blob/main/images/stirling-home.jpg"><img src="/Stirling-Tools/Stirling-PDF/raw/main/images/stirling-home.jpg" alt="斯特林主页" style="max-width: 100%;"></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">特征</font></font></h2><a id="user-content-features" class="anchor" aria-label="固定链接：功能" href="#features"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持暗黑模式。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自定义下载选项（参见</font></font><a href="https://github.com/Stirling-Tools/Stirling-PDF/blob/main/images/settings.png"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">此处</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">的示例）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">并行文件处理和下载</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于与外部脚本集成的 API</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可选的登录和身份验证支持（请参阅</font></font><a href="https://github.com/Stirling-Tools/Stirling-PDF/tree/main#login-authentication"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">此处</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">的文档）</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PDF 功能</font></font></strong></h2><a id="user-content-pdf-features" class="anchor" aria-label="固定链接：PDF 功能" href="#pdf-features"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">页面操作</font></font></strong></h3><a id="user-content-page-operations" class="anchor" aria-label="固定链接：页面操作" href="#page-operations"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">查看和修改 PDF - 使用自定义查看排序和搜索查看多页 PDF。此外还有页面编辑功能，如注释、绘制和添加文本和图像。（使用 PDF.js 和 Joxit 和 Liberation.Liberation 字体）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于合并/分割/旋转/移动 PDF 及其页面的完整交互式 GUI。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将多个 PDF 合并为一个结果文件。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">按照指定页码将 PDF 拆分为多个文件或将所有页面提取为单独的文件。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将 PDF 页面重新排列为不同的顺序。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以 90 度为增量旋转 PDF。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">刪除頁面。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多页布局（将 PDF 格式化为多页页面）。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">按设置的百分比缩放页面内容大小。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">调整对比度。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">裁剪 PDF。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自动分割 PDF（带有物理扫描的页面分隔符）。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提取页面。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将 PDF 转换为单页。</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">转换操作</font></font></strong></h3><a id="user-content-conversion-operations" class="anchor" aria-label="固定链接：转换操作" href="#conversion-operations"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将 PDF 转换为图像，或将 PDF 转换为图像。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将任何常见文件转换为 PDF（使用 LibreOffice）。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将 PDF 转换为 Word/Powerpoint/其他（使用 LibreOffice）。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将 HTML 转换为 PDF。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PDF 的 URL。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Markdown 转 PDF。</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安全和权限</font></font></strong></h3><a id="user-content-security--permissions" class="anchor" aria-label="固定链接：安全和权限" href="#security--permissions"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">添加和删&ZeroWidthSpace;&ZeroWidthSpace;除密码。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">更改/设置 PDF 权限。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">添加水印。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">认证/签署 PDF。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">净化 PDF。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自动编辑文本。</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">其他操作</font></font></strong></h3><a id="user-content-other-operations" class="anchor" aria-label="永久链接：其他操作" href="#other-operations"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">添加/生成/写入签名。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">修复 PDF。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">检测并删除空白页。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">比较 2 个 PDF 并显示文本中的差异。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将图像添加到 PDF。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">压缩 PDF 以减小其文件大小（使用 OCRMyPDF）。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从 PDF 中提取图像。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从扫描中提取图像。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">添加页码。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过检测 PDF 标题文本自动重命名文件。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PDF 上的 OCR（使用 OCRMyPDF）。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PDF/A 转换（使用 OCRMyPDF）。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">编辑元数据。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">扁平化 PDF。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">获取 PDF 上的所有信息以查看或导出为 JSON。</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有关每个任务和技术的概述，请查看</font></font><a href="https://github.com/Stirling-Tools/Stirling-PDF/blob/main/Endpoint-groups.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Endpoint-groups.md</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
应用程序的演示可</font></font><a href="https://stirlingpdf.io" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在此处</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">获得。用户名：demo，密码：demo</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用的技术</font></font></h2><a id="user-content-technologies-used" class="anchor" aria-label="永久链接：所用技术" href="#technologies-used"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Spring Boot + Thymeleaf</font></font></li>
<li><a href="https://github.com/apache/pdfbox/tree/trunk"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PDF 框</font></font></a></li>
<li><a href="https://www.libreoffice.org/discover/libreoffice/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LibreOffice</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于高级转换</font></font></li>
<li><a href="https://github.com/ocrmypdf/OCRmyPDF"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">奥克迈普</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HTML、CSS、JavaScript</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker</font></font></li>
<li><a href="https://github.com/mozilla/pdf.js"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PDF.js</font></font></a></li>
<li><a href="https://github.com/Hopding/pdf-lib"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PDF-LIB.js</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何使用</font></font></h2><a id="user-content-how-to-use" class="anchor" aria-label="固定链接：如何使用" href="#how-to-use"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">本地</font></font></h3><a id="user-content-locally" class="anchor" aria-label="永久链接：本地" href="#locally"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请查看</font></font><a href="https://github.com/Stirling-Tools/Stirling-PDF/blob/main/LocalRunGuide.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/Stirling-Tools/Stirling-PDF/blob/main/LocalRunGuide.md</font></font></a></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker/Podman</font></font></h3><a id="user-content-docker--podman" class="anchor" aria-label="永久链接：Docker / Podman" href="#docker--podman"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><a href="https://hub.docker.com/r/frooodle/s-pdf" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://hub.docker.com/r/frooodle/s-pdf</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Stirling PDF 有两个不同版本，一个完整版和一个超精简版。根据您使用的功能类型，您可能需要较小的图像以节省空间。要查看不同版本提供的内容，请查看我们的</font></font><a href="https://github.com/Stirling-Tools/Stirling-PDF/blob/main/Version-groups.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">版本映射。</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
对于不介意空间优化的人，只需使用最新标签即可。
</font></font><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/fbd9884966c7b2cb6df6cdb0ce00df746aee87215f1395da3f79d52f5c2ff622/68747470733a2f2f696d672e736869656c64732e696f2f646f636b65722f696d6167652d73697a652f66726f6f6f646c652f732d7064662f6c61746573743f6c6162656c3d537469726c696e672d50444625323046756c6c"><img src="https://camo.githubusercontent.com/fbd9884966c7b2cb6df6cdb0ce00df746aee87215f1395da3f79d52f5c2ff622/68747470733a2f2f696d672e736869656c64732e696f2f646f636b65722f696d6167652d73697a652f66726f6f6f646c652f732d7064662f6c61746573743f6c6162656c3d537469726c696e672d50444625323046756c6c" alt="Docker 镜像大小（标签）" data-canonical-src="https://img.shields.io/docker/image-size/frooodle/s-pdf/latest?label=Stirling-PDF%20Full" style="max-width: 100%;"></a>
<a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/a3d47e7dc39ea0473c5d476cf4b1d600a97755154b923467a49526e7055b67ed/68747470733a2f2f696d672e736869656c64732e696f2f646f636b65722f696d6167652d73697a652f66726f6f6f646c652f732d7064662f6c61746573742d756c7472612d6c6974653f6c6162656c3d537469726c696e672d504446253230556c7472612d4c697465"><img src="https://camo.githubusercontent.com/a3d47e7dc39ea0473c5d476cf4b1d600a97755154b923467a49526e7055b67ed/68747470733a2f2f696d672e736869656c64732e696f2f646f636b65722f696d6167652d73697a652f66726f6f6f646c652f732d7064662f6c61746573742d756c7472612d6c6974653f6c6162656c3d537469726c696e672d504446253230556c7472612d4c697465" alt="Docker 镜像大小（标签）" data-canonical-src="https://img.shields.io/docker/image-size/frooodle/s-pdf/latest-ultra-lite?label=Stirling-PDF%20Ultra-Lite" style="max-width: 100%;"></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 运行</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>docker run -d \
  -p 8080:8080 \
  -v /location/of/trainingData:/usr/share/tessdata \
  -v /location/of/extraConfigs:/configs \
  -v /location/of/logs:/logs \
  -e DOCKER_ENABLE_SECURITY=false \
  -e INSTALL_BOOK_AND_ADVANCED_HTML_OPS=false \
  -e LANGS=en_GB \
  --name stirling-pdf \
  frooodle/s-pdf:latest


  Can also add these <span class="pl-k">for</span> customisation but are not required

  -v /location/of/customFiles:/customFiles \</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="docker run -d \
  -p 8080:8080 \
  -v /location/of/trainingData:/usr/share/tessdata \
  -v /location/of/extraConfigs:/configs \
  -v /location/of/logs:/logs \
  -e DOCKER_ENABLE_SECURITY=false \
  -e INSTALL_BOOK_AND_ADVANCED_HTML_OPS=false \
  -e LANGS=en_GB \
  --name stirling-pdf \
  frooodle/s-pdf:latest


  Can also add these for customisation but are not required

  -v /location/of/customFiles:/customFiles \" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 组成</font></font></p>
<div class="highlight highlight-source-yaml notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-ent">version</span>: <span class="pl-s"><span class="pl-pds">'</span>3.3<span class="pl-pds">'</span></span>
<span class="pl-ent">services</span>:
  <span class="pl-ent">stirling-pdf</span>:
    <span class="pl-ent">image</span>: <span class="pl-s">frooodle/s-pdf:latest</span>
    <span class="pl-ent">ports</span>:
      - <span class="pl-s"><span class="pl-pds">'</span>8080:8080<span class="pl-pds">'</span></span>
    <span class="pl-ent">volumes</span>:
      - <span class="pl-s">/location/of/trainingData:/usr/share/tessdata </span><span class="pl-c"><span class="pl-c">#</span>Required for extra OCR languages</span>
      - <span class="pl-s">/location/of/extraConfigs:/configs</span>
<span class="pl-c"><span class="pl-c">#</span>      - /location/of/customFiles:/customFiles/</span>
<span class="pl-c"><span class="pl-c">#</span>      - /location/of/logs:/logs/</span>
    <span class="pl-ent">environment</span>:
      - <span class="pl-s">DOCKER_ENABLE_SECURITY=false</span>
      - <span class="pl-s">INSTALL_BOOK_AND_ADVANCED_HTML_OPS=false</span>
      - <span class="pl-s">LANGS=en_GB</span></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="version: '3.3'
services:
  stirling-pdf:
    image: frooodle/s-pdf:latest
    ports:
      - '8080:8080'
    volumes:
      - /location/of/trainingData:/usr/share/tessdata #Required for extra OCR languages
      - /location/of/extraConfigs:/configs
#      - /location/of/customFiles:/customFiles/
#      - /location/of/logs:/logs/
    environment:
      - DOCKER_ENABLE_SECURITY=false
      - INSTALL_BOOK_AND_ADVANCED_HTML_OPS=false
      - LANGS=en_GB" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">注意：Podman 与 Docker CLI 兼容，因此只需将“docker”替换为“podman”即可。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">启用 OCR/压缩功能</font></font></h2><a id="user-content-enable-ocrcompression-feature" class="anchor" aria-label="永久链接：启用 OCR/压缩功能" href="#enable-ocrcompression-feature"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请查看</font></font><a href="https://github.com/Stirling-Tools/Stirling-PDF/blob/main/HowToUseOCR.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/Stirling-Tools/Stirling-PDF/blob/main/HowToUseOCR.md</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持的语言</font></font></h2><a id="user-content-supported-languages" class="anchor" aria-label="固定链接：支持的语言" href="#supported-languages"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Stirling PDF目前支持32！</font></font></p>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">语言</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">进步</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">英语（英语）（en_GB）</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/1807370956e6a5ca096d77997b25186acdd92b1a3573490cd9f2bf8395438ef5/68747470733a2f2f676570732e6465762f70726f67726573732f313030"><img src="https://camo.githubusercontent.com/1807370956e6a5ca096d77997b25186acdd92b1a3573490cd9f2bf8395438ef5/68747470733a2f2f676570732e6465762f70726f67726573732f313030" alt="100％" data-canonical-src="https://geps.dev/progress/100" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">英语（美国）（en_US）</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/1807370956e6a5ca096d77997b25186acdd92b1a3573490cd9f2bf8395438ef5/68747470733a2f2f676570732e6465762f70726f67726573732f313030"><img src="https://camo.githubusercontent.com/1807370956e6a5ca096d77997b25186acdd92b1a3573490cd9f2bf8395438ef5/68747470733a2f2f676570732e6465762f70726f67726573732f313030" alt="100％" data-canonical-src="https://geps.dev/progress/100" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阿拉伯语（العربية）（ar_AR）</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/94b9fefa498225c9a26851f1b561ba93bd69dacf1b6acdeadd3303ff0ad1fa69/68747470733a2f2f676570732e6465762f70726f67726573732f3430"><img src="https://camo.githubusercontent.com/94b9fefa498225c9a26851f1b561ba93bd69dacf1b6acdeadd3303ff0ad1fa69/68747470733a2f2f676570732e6465762f70726f67726573732f3430" alt="40％" data-canonical-src="https://geps.dev/progress/40" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">德语（Deutsch） (de_DE)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/1807370956e6a5ca096d77997b25186acdd92b1a3573490cd9f2bf8395438ef5/68747470733a2f2f676570732e6465762f70726f67726573732f313030"><img src="https://camo.githubusercontent.com/1807370956e6a5ca096d77997b25186acdd92b1a3573490cd9f2bf8395438ef5/68747470733a2f2f676570732e6465762f70726f67726573732f313030" alt="100％" data-canonical-src="https://geps.dev/progress/100" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法语（Français）（fr_FR）</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/2ca9eb613a743cd6e4aeec3eb180384d442ca1d8aae9a72639cea40b5b6f1e3d/68747470733a2f2f676570732e6465762f70726f67726573732f3935"><img src="https://camo.githubusercontent.com/2ca9eb613a743cd6e4aeec3eb180384d442ca1d8aae9a72639cea40b5b6f1e3d/68747470733a2f2f676570732e6465762f70726f67726573732f3935" alt="95％" data-canonical-src="https://geps.dev/progress/95" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">西班牙语（Español）（es_ES）</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/2ca9eb613a743cd6e4aeec3eb180384d442ca1d8aae9a72639cea40b5b6f1e3d/68747470733a2f2f676570732e6465762f70726f67726573732f3935"><img src="https://camo.githubusercontent.com/2ca9eb613a743cd6e4aeec3eb180384d442ca1d8aae9a72639cea40b5b6f1e3d/68747470733a2f2f676570732e6465762f70726f67726573732f3935" alt="95％" data-canonical-src="https://geps.dev/progress/95" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">简体中文 (zh_CN)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/2ca9eb613a743cd6e4aeec3eb180384d442ca1d8aae9a72639cea40b5b6f1e3d/68747470733a2f2f676570732e6465762f70726f67726573732f3935"><img src="https://camo.githubusercontent.com/2ca9eb613a743cd6e4aeec3eb180384d442ca1d8aae9a72639cea40b5b6f1e3d/68747470733a2f2f676570732e6465762f70726f67726573732f3935" alt="95％" data-canonical-src="https://geps.dev/progress/95" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">繁體中文 (zh_TW)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/571e721777720b5548a7994612dde203ee1ef9ca3cd2a5406fa7334d0b298457/68747470733a2f2f676570732e6465762f70726f67726573732f3934"><img src="https://camo.githubusercontent.com/571e721777720b5548a7994612dde203ee1ef9ca3cd2a5406fa7334d0b298457/68747470733a2f2f676570732e6465762f70726f67726573732f3934" alt="94％" data-canonical-src="https://geps.dev/progress/94" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">加泰罗尼亚语（Català）（ca_CA）</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/b8a96d411f443a0c9914e55adc2066fbe22fd9f64b979842d92ba385d7a87a08/68747470733a2f2f676570732e6465762f70726f67726573732f3439"><img src="https://camo.githubusercontent.com/b8a96d411f443a0c9914e55adc2066fbe22fd9f64b979842d92ba385d7a87a08/68747470733a2f2f676570732e6465762f70726f67726573732f3439" alt="49％" data-canonical-src="https://geps.dev/progress/49" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">意大利语（Italiano）（it_IT）</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/56ca0e85731a93838f4c7bced8c256e4dd1300c0bdada0fc981d74a9913b1717/68747470733a2f2f676570732e6465762f70726f67726573732f3939"><img src="https://camo.githubusercontent.com/56ca0e85731a93838f4c7bced8c256e4dd1300c0bdada0fc981d74a9913b1717/68747470733a2f2f676570732e6465762f70726f67726573732f3939" alt="99％" data-canonical-src="https://geps.dev/progress/99" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">瑞典语（Svenska）（sv_SE）</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/94b9fefa498225c9a26851f1b561ba93bd69dacf1b6acdeadd3303ff0ad1fa69/68747470733a2f2f676570732e6465762f70726f67726573732f3430"><img src="https://camo.githubusercontent.com/94b9fefa498225c9a26851f1b561ba93bd69dacf1b6acdeadd3303ff0ad1fa69/68747470733a2f2f676570732e6465762f70726f67726573732f3430" alt="40％" data-canonical-src="https://geps.dev/progress/40" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">波兰语（Polski） (pl_PL)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/b8e040de43b795adfdd7ce0c18fb3159a8c66b41163fec5a54097afa6f99281e/68747470733a2f2f676570732e6465762f70726f67726573732f3432"><img src="https://camo.githubusercontent.com/b8e040de43b795adfdd7ce0c18fb3159a8c66b41163fec5a54097afa6f99281e/68747470733a2f2f676570732e6465762f70726f67726573732f3432" alt="42%" data-canonical-src="https://geps.dev/progress/42" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">罗马尼亚语 (Română) (ro_RO)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/a17bac363cfd9c3b26261a28a52e81029111f38d464afef269e93b2e447bee09/68747470733a2f2f676570732e6465762f70726f67726573732f3339"><img src="https://camo.githubusercontent.com/a17bac363cfd9c3b26261a28a52e81029111f38d464afef269e93b2e447bee09/68747470733a2f2f676570732e6465762f70726f67726573732f3339" alt="39%" data-canonical-src="https://geps.dev/progress/39" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">韩语（한국어）（ko_KR）</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/6b76a696604f14731725afbd1edef0ac320f6b9a17f5c66cfdf79ff5865388d1/68747470733a2f2f676570732e6465762f70726f67726573732f3837"><img src="https://camo.githubusercontent.com/6b76a696604f14731725afbd1edef0ac320f6b9a17f5c66cfdf79ff5865388d1/68747470733a2f2f676570732e6465762f70726f67726573732f3837" alt="87％" data-canonical-src="https://geps.dev/progress/87" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">巴西葡萄牙语 (Português) (pt_BR)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/13f50651b5073929ae04dd954125396827524ed7aab9d0857924e12fbb26b377/68747470733a2f2f676570732e6465762f70726f67726573732f3631"><img src="https://camo.githubusercontent.com/13f50651b5073929ae04dd954125396827524ed7aab9d0857924e12fbb26b377/68747470733a2f2f676570732e6465762f70726f67726573732f3631" alt="61%" data-canonical-src="https://geps.dev/progress/61" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">俄语 (Русский) (ru_RU)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/6b76a696604f14731725afbd1edef0ac320f6b9a17f5c66cfdf79ff5865388d1/68747470733a2f2f676570732e6465762f70726f67726573732f3837"><img src="https://camo.githubusercontent.com/6b76a696604f14731725afbd1edef0ac320f6b9a17f5c66cfdf79ff5865388d1/68747470733a2f2f676570732e6465762f70726f67726573732f3837" alt="87％" data-canonical-src="https://geps.dev/progress/87" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">巴斯克语（Euskara）(eu_ES)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/844cd325c4f7059af58e2349ed48a1653388a6e101f56365a76240ae870320de/68747470733a2f2f676570732e6465762f70726f67726573732f3633"><img src="https://camo.githubusercontent.com/844cd325c4f7059af58e2349ed48a1653388a6e101f56365a76240ae870320de/68747470733a2f2f676570732e6465762f70726f67726573732f3633" alt="63%" data-canonical-src="https://geps.dev/progress/63" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">日语（日本語） (ja_JP)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/6b76a696604f14731725afbd1edef0ac320f6b9a17f5c66cfdf79ff5865388d1/68747470733a2f2f676570732e6465762f70726f67726573732f3837"><img src="https://camo.githubusercontent.com/6b76a696604f14731725afbd1edef0ac320f6b9a17f5c66cfdf79ff5865388d1/68747470733a2f2f676570732e6465762f70726f67726573732f3837" alt="87％" data-canonical-src="https://geps.dev/progress/87" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">荷兰语（Nederlands）(nl_NL)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/a50173c39c2e2b34093e2e1c2b434a018d585468618a041f22bd2389dd9b56bd/68747470733a2f2f676570732e6465762f70726f67726573732f3835"><img src="https://camo.githubusercontent.com/a50173c39c2e2b34093e2e1c2b434a018d585468618a041f22bd2389dd9b56bd/68747470733a2f2f676570732e6465762f70726f67726573732f3835" alt="85％" data-canonical-src="https://geps.dev/progress/85" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">希腊语 (Ελληνικά) (el_GR)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/a50173c39c2e2b34093e2e1c2b434a018d585468618a041f22bd2389dd9b56bd/68747470733a2f2f676570732e6465762f70726f67726573732f3835"><img src="https://camo.githubusercontent.com/a50173c39c2e2b34093e2e1c2b434a018d585468618a041f22bd2389dd9b56bd/68747470733a2f2f676570732e6465762f70726f67726573732f3835" alt="85％" data-canonical-src="https://geps.dev/progress/85" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">土耳其语（Türkçe）(tr_TR)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/843606550bfaa4dc4119a3babe89a624f02fec12c66c6182c7a59a6371c3b9fa/68747470733a2f2f676570732e6465762f70726f67726573732f3937"><img src="https://camo.githubusercontent.com/843606550bfaa4dc4119a3babe89a624f02fec12c66c6182c7a59a6371c3b9fa/68747470733a2f2f676570732e6465762f70726f67726573732f3937" alt="97％" data-canonical-src="https://geps.dev/progress/97" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">印度尼西亚（印尼语）(id_ID)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/ed36b9ab23eddbe31a7faa8949aff84b89551d662dc80860d56a00bac351f3ef/68747470733a2f2f676570732e6465762f70726f67726573732f3738"><img src="https://camo.githubusercontent.com/ed36b9ab23eddbe31a7faa8949aff84b89551d662dc80860d56a00bac351f3ef/68747470733a2f2f676570732e6465762f70726f67726573732f3738" alt="78％" data-canonical-src="https://geps.dev/progress/78" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">印地语（हिंदी）（hi_IN）</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/c734240a6afcba13af10d5e00c997b1cb49946bb9e389bc596d5f9d92ad996c0/68747470733a2f2f676570732e6465762f70726f67726573732f3739"><img src="https://camo.githubusercontent.com/c734240a6afcba13af10d5e00c997b1cb49946bb9e389bc596d5f9d92ad996c0/68747470733a2f2f676570732e6465762f70726f67726573732f3739" alt="79％" data-canonical-src="https://geps.dev/progress/79" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">匈牙利语（Magyar） (hu_HU)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/ed36b9ab23eddbe31a7faa8949aff84b89551d662dc80860d56a00bac351f3ef/68747470733a2f2f676570732e6465762f70726f67726573732f3738"><img src="https://camo.githubusercontent.com/ed36b9ab23eddbe31a7faa8949aff84b89551d662dc80860d56a00bac351f3ef/68747470733a2f2f676570732e6465762f70726f67726573732f3738" alt="78％" data-canonical-src="https://geps.dev/progress/78" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">保加利亚语 (Български) (bg_BG)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/c2f4dd8999790b4cebd8dde128a78dee462cb59da66df7e5504eae3c5d223bd3/68747470733a2f2f676570732e6465762f70726f67726573732f3938"><img src="https://camo.githubusercontent.com/c2f4dd8999790b4cebd8dde128a78dee462cb59da66df7e5504eae3c5d223bd3/68747470733a2f2f676570732e6465762f70726f67726573732f3938" alt="98％" data-canonical-src="https://geps.dev/progress/98" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">塞比亚拉丁字母 (Srpski) (sr_LATN_RS)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/7cf0ee48bb8ccc5e439f2a98779938f3304dc7d5b5ccb9d7fcfc6448475a2637/68747470733a2f2f676570732e6465762f70726f67726573732f3830"><img src="https://camo.githubusercontent.com/7cf0ee48bb8ccc5e439f2a98779938f3304dc7d5b5ccb9d7fcfc6448475a2637/68747470733a2f2f676570732e6465762f70726f67726573732f3830" alt="80％" data-canonical-src="https://geps.dev/progress/80" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">乌克兰语 (Українська) (uk_UA)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/b0a5ec07fca11b2964dcfe455335c315b6628b3c6e1538f8b3cffadb3f378d0e/68747470733a2f2f676570732e6465762f70726f67726573732f3836"><img src="https://camo.githubusercontent.com/b0a5ec07fca11b2964dcfe455335c315b6628b3c6e1538f8b3cffadb3f378d0e/68747470733a2f2f676570732e6465762f70726f67726573732f3836" alt="86%" data-canonical-src="https://geps.dev/progress/86" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">斯洛伐克语（Slovensky） (sk_SK)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/2ca9eb613a743cd6e4aeec3eb180384d442ca1d8aae9a72639cea40b5b6f1e3d/68747470733a2f2f676570732e6465762f70726f67726573732f3935"><img src="https://camo.githubusercontent.com/2ca9eb613a743cd6e4aeec3eb180384d442ca1d8aae9a72639cea40b5b6f1e3d/68747470733a2f2f676570732e6465762f70726f67726573732f3935" alt="95％" data-canonical-src="https://geps.dev/progress/95" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">捷克语（Česky）（cs_CZ）</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/695b9f5378ba21b732b757099058b71f4c7ba2157f42e409bfc9af6b198f8732/68747470733a2f2f676570732e6465762f70726f67726573732f3933"><img src="https://camo.githubusercontent.com/695b9f5378ba21b732b757099058b71f4c7ba2157f42e409bfc9af6b198f8732/68747470733a2f2f676570732e6465762f70726f67726573732f3933" alt="93％" data-canonical-src="https://geps.dev/progress/93" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">克罗地亚语（Hrvatski）(hr_HR)</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/c2f4dd8999790b4cebd8dde128a78dee462cb59da66df7e5504eae3c5d223bd3/68747470733a2f2f676570732e6465762f70726f67726573732f3938"><img src="https://camo.githubusercontent.com/c2f4dd8999790b4cebd8dde128a78dee462cb59da66df7e5504eae3c5d223bd3/68747470733a2f2f676570732e6465762f70726f67726573732f3938" alt="98％" data-canonical-src="https://geps.dev/progress/98" style="max-width: 100%;"></a></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">挪威语（Norsk）（no_NB）</font></font></td>
<td><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/c2f4dd8999790b4cebd8dde128a78dee462cb59da66df7e5504eae3c5d223bd3/68747470733a2f2f676570732e6465762f70726f67726573732f3938"><img src="https://camo.githubusercontent.com/c2f4dd8999790b4cebd8dde128a78dee462cb59da66df7e5504eae3c5d223bd3/68747470733a2f2f676570732e6465762f70726f67726573732f3938" alt="98％" data-canonical-src="https://geps.dev/progress/98" style="max-width: 100%;"></a></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">贡献（创建问题、翻译、修复错误等）</font></font></h2><a id="user-content-contributing-creating-issues-translations-fixing-bugs-etc" class="anchor" aria-label="永久链接：贡献（创建问题、翻译、修复错误等）" href="#contributing-creating-issues-translations-fixing-bugs-etc"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请参阅我们的</font></font><a href="/Stirling-Tools/Stirling-PDF/blob/main/CONTRIBUTING.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">贡献指南</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">！</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">定制</font></font></h2><a id="user-content-customisation" class="anchor" aria-label="固定链接：定制" href="#customisation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Stirling PDF 可轻松定制应用程序。包括以下内容</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自定义应用程序名称</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自定义标语、图标、HTML、图像 CSS 等（通过文件覆盖）</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有两个选项，要么使用生成的设置文件，</font></font><code>settings.yml</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
该文件位于</font></font><code>/configs</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">目录中，并遵循标准 YAML 格式</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">环境变量也受支持，并将覆盖设置文件，例如在 settings.yml 中你有</font></font></p>
<div class="highlight highlight-source-yaml notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-ent">system</span>:
  <span class="pl-ent">enableLogin</span>: <span class="pl-s"><span class="pl-pds">'</span>true<span class="pl-pds">'</span></span></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="system:
  enableLogin: 'true'" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">要通过环境变量来实现这一点你需要</font></font><code>SYSTEM_ENABLELOGIN</code></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">当前设置列表为</font></font></p>
<div class="highlight highlight-source-yaml notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-ent">security</span>:
  <span class="pl-ent">enableLogin</span>: <span class="pl-s">false </span><span class="pl-c"><span class="pl-c">#</span> set to 'true' to enable login</span>
  <span class="pl-ent">csrfDisabled</span>: <span class="pl-s">true </span><span class="pl-c"><span class="pl-c">#</span> Set to 'true' to disable CSRF protection (not recommended for production)</span>
  <span class="pl-ent">loginAttemptCount</span>: <span class="pl-c1">5</span> <span class="pl-c"><span class="pl-c">#</span> lock user account after 5 tries</span>
  <span class="pl-ent">loginResetTimeMinutes</span>: <span class="pl-c1">120</span> <span class="pl-c"><span class="pl-c">#</span> lock account for 2 hours after x attempts</span>
<span class="pl-c"><span class="pl-c">#</span>  initialLogin:</span>
<span class="pl-c"><span class="pl-c">#</span>    username: "admin" # Initial username for the first login</span>
<span class="pl-c"><span class="pl-c">#</span>    password: "stirling" # Initial password for the first login</span>
<span class="pl-c"><span class="pl-c">#</span>  oauth2:</span>
<span class="pl-c"><span class="pl-c">#</span>    enabled: false # set to 'true' to enable login (Note: enableLogin must also be 'true' for this to work)</span>
<span class="pl-c"><span class="pl-c">#</span>    issuer: "" # set to any provider that supports OpenID Connect Discovery (/.well-known/openid-configuration) end-point</span>
<span class="pl-c"><span class="pl-c">#</span>    clientId: "" # Client ID from your provider</span>
<span class="pl-c"><span class="pl-c">#</span>    clientSecret: "" # Client Secret from your provider</span>
<span class="pl-c"><span class="pl-c">#</span>    autoCreateUser: false # set to 'true' to allow auto-creation of non-existing users</span>
<span class="pl-c"><span class="pl-c">#</span>    useAsUsername: "email" # Default is 'email'; custom fields can be used as the username</span>
<span class="pl-c"><span class="pl-c">#</span>    scopes: "openid, profile, email" # Specify the scopes for which the application will request permissions</span>
<span class="pl-c"><span class="pl-c">#</span>    provider: "google" # Set this to your OAuth provider's name, e.g., 'google' or 'keycloak'</span>
<span class="pl-c"><span class="pl-c">#</span>    client:</span>
<span class="pl-c"><span class="pl-c">#</span>      google:</span>
<span class="pl-c"><span class="pl-c">#</span>        clientId: "" # Client ID for Google OAuth2</span>
<span class="pl-c"><span class="pl-c">#</span>        clientSecret: "" # Client Secret for Google OAuth2</span>
<span class="pl-c"><span class="pl-c">#</span>        scopes: "https://www.googleapis.com/auth/userinfo.email, https://www.googleapis.com/auth/userinfo.profile" # Scopes for Google OAuth2</span>
<span class="pl-c"><span class="pl-c">#</span>        useAsUsername: "email" # Field to use as the username for Google OAuth2</span>
<span class="pl-c"><span class="pl-c">#</span>      github:</span>
<span class="pl-c"><span class="pl-c">#</span>        clientId: "" # Client ID for GitHub OAuth2</span>
<span class="pl-c"><span class="pl-c">#</span>        clientSecret: "" # Client Secret for GitHub OAuth2</span>
<span class="pl-c"><span class="pl-c">#</span>        scopes: "read:user" # Scope for GitHub OAuth2</span>
<span class="pl-c"><span class="pl-c">#</span>        useAsUsername: "login" # Field to use as the username for GitHub OAuth2</span>
<span class="pl-c"><span class="pl-c">#</span>      keycloak:</span>
<span class="pl-c"><span class="pl-c">#</span>        issuer: "http://192.168.0.123:8888/realms/stirling-pdf" # URL of the Keycloak realm's OpenID Connect Discovery endpoint</span>
<span class="pl-c"><span class="pl-c">#</span>        clientId: "stirling-pdf" # Client ID for Keycloak OAuth2</span>
<span class="pl-c"><span class="pl-c">#</span>        clientSecret: "" # Client Secret for Keycloak OAuth2</span>
<span class="pl-c"><span class="pl-c">#</span>        scopes: "openid, profile, email" # Scopes for Keycloak OAuth2</span>
<span class="pl-c"><span class="pl-c">#</span>        useAsUsername: "email" # Field to use as the username for Keycloak OAuth2</span>

<span class="pl-ent">system</span>:
  <span class="pl-ent">defaultLocale</span>: <span class="pl-s"><span class="pl-pds">'</span>en-US<span class="pl-pds">'</span></span> <span class="pl-c"><span class="pl-c">#</span> Set the default language (e.g. 'de-DE', 'fr-FR', etc)</span>
  <span class="pl-ent">googlevisibility</span>: <span class="pl-s">false </span><span class="pl-c"><span class="pl-c">#</span> 'true' to allow Google visibility (via robots.txt), 'false' to disallow</span>
  <span class="pl-ent">enableAlphaFunctionality</span>: <span class="pl-s">false </span><span class="pl-c"><span class="pl-c">#</span> Set to enable functionality which might need more testing before it fully goes live (This feature might make no changes)</span>
  <span class="pl-ent">showUpdate</span>: <span class="pl-s">true </span><span class="pl-c"><span class="pl-c">#</span> see when a new update is available</span>
  <span class="pl-ent">showUpdateOnlyAdmin</span>: <span class="pl-s">false </span><span class="pl-c"><span class="pl-c">#</span> Only admins can see when a new update is available, depending on showUpdate it must be set to 'true'</span>
  <span class="pl-ent">customHTMLFiles</span>: <span class="pl-s">false </span><span class="pl-c"><span class="pl-c">#</span> enable to have files placed in /customFiles/templates override the existing template html files</span>

<span class="pl-ent">ui</span>:
  <span class="pl-ent">appName</span>: <span class="pl-s">null </span><span class="pl-c"><span class="pl-c">#</span> Application's visible name</span>
  <span class="pl-ent">homeDescription</span>: <span class="pl-s">null </span><span class="pl-c"><span class="pl-c">#</span> Short description or tagline shown on homepage.</span>
  <span class="pl-ent">appNameNavbar</span>: <span class="pl-s">null </span><span class="pl-c"><span class="pl-c">#</span> Name displayed on the navigation bar</span>

<span class="pl-ent">endpoints</span>:
  <span class="pl-ent">toRemove</span>: <span class="pl-s">[] </span><span class="pl-c"><span class="pl-c">#</span> List endpoints to disable (e.g. ['img-to-pdf', 'remove-pages'])</span>
  <span class="pl-ent">groupsToRemove</span>: <span class="pl-s">[] </span><span class="pl-c"><span class="pl-c">#</span> List groups to disable (e.g. ['LibreOffice'])</span>

<span class="pl-ent">metrics</span>:
  <span class="pl-ent">enabled</span>: <span class="pl-s">true </span><span class="pl-c"><span class="pl-c">#</span> 'true' to enable Info APIs (`/api/*`) endpoints, 'false' to disable</span></pre><div class="zeroclipboard-container">
 
<li><code>SYSTEM_ROOTURIPATH</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">即设置为</font></font><code>/pdf-app</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将应用程序的根 URI 设置为</font></font><code>localhost:8080/pdf-app</code></li>
<li><code>SYSTEM_CONNECTIONTIMEOUTMINUTES</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">设置自定义连接超时值</font></font></li>
<li><code>DOCKER_ENABLE_SECURITY</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">告诉 docker 下载安全 jar（对于身份验证登录，必须为 true）</font></font></li>
<li><code>INSTALL_BOOK_AND_ADVANCED_HTML_OPS</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将 calibre 下载到 stirling-pdf 上，实现 pdf 与书籍之间的相互转换以及高级 html 转换</font></font></li>
<li><code>LANGS</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">定义要安装的自定义字体库以用于文档转换</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">API</font></font></h2><a id="user-content-api" class="anchor" aria-label="固定链接：API" href="#api"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对于那些想要使用 Stirling-PDFs 后端 API 链接到他们自己的自定义脚本来编辑 PDF 的人，您可以
</font></font><a href="https://app.swaggerhub.com/apis-docs/Stirling-Tools/Stirling-PDF/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在此处</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">查看所有现有的 API 文档，或导航到您的 stirling-pdf 实例的 /swagger-ui/index.html 获取您的版本文档（或者按照 Stirling-PDF 设置中的 API 按钮）</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">登录认证</font></font></h2><a id="user-content-login-authentication" class="anchor" aria-label="永久链接：登录认证" href="#login-authentication"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><a target="_blank" rel="noopener noreferrer" href="/Stirling-Tools/Stirling-PDF/blob/main/images/login-light.png"><img src="/Stirling-Tools/Stirling-PDF/raw/main/images/login-light.png" alt="斯特林登录" style="max-width: 100%;"></a></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">先决条件：</font></font></h3><a id="user-content-prerequisites" class="anchor" aria-label="永久链接：先决条件：" href="#prerequisites"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用户必须在 docker 中拥有 ./configs 卷，以便在更新期间保留它。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 用户必须通过设置环境变量</font></font><code>DOCKER_ENABLE_SECURITY</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">来</font><font style="vertical-align: inherit;">下载安全 jar 版本。</font></font><code>true</code><font style="vertical-align: inherit;"></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">然后通过 settings.yml 文件或通过设置启用</font></font><code>SECURITY_ENABLE_LOGIN</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">登录</font></font><code>true</code></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">现在将生成具有用户名</font></font><code>admin</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和密码的初始用户</font></font><code>stirling</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。登录时，您将被迫将密码更改为新密码。您还可以使用环境变量</font></font><code>SECURITY_INITIALLOGIN_USERNAME</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和  </font></font><code>SECURITY_INITIALLOGIN_PASSWORD</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">立即设置自己的环境变量（建议在创建用户后将其删除）。</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">完成上述操作后，重新启动时，如果一切正常，将显示新的 stirling-pdf-DB.mv.db。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">当您登录 Stirling PDF 时，您将被重定向到 /login 页面，以使用这些默认凭据登录。登录后，一切都应正常运行</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">要访问您的帐户设置，请转到设置齿轮菜单（导航栏右上角）中的帐户设置，您也可以在此帐户设置菜单中找到您的 API 密钥。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">要添加新用户，请转到帐户设置底部并点击“管理员设置”，在这里您可以添加新用户。此处提到的不同角色用于限制速率。这是一项正在进行的工作，将来会进一步扩展</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对于 API 的使用，您必须提供带有“X-API-Key”的标题以及与该用户关联的 API 密钥。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">常问问题</font></font></h2><a id="user-content-faq" class="anchor" aria-label="永久链接：常见问题解答" href="#faq"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Q1：您计划的功能有哪些？</font></font></h3><a id="user-content-q1-what-are-your-planned-features" class="anchor" aria-label="永久链接：问题 1：您计划的功能有哪些？" href="#q1-what-are-your-planned-features"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">进度条/跟踪</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">完全自定义的逻辑管道将多个操作组合在一起。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持文件夹自动扫描以执行操作</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">编辑文本（通过用户界面而非自动化方式）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">添加表单</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多页面布局（将 PDF 页面拼接在一起）支持 x 行 y 列和自定义页面大小</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">手动或自动填写表格</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">问题 2：为什么我的应用程序下载 .htm 文件？</font></font></h3><a id="user-content-q2-why-is-my-application-downloading-htm-files" class="anchor" aria-label="永久链接：问题 2：为什么我的应用程序正在下载 .htm 文件？" href="#q2-why-is-my-application-downloading-htm-files"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这是由您的 NGINX 配置通常引起的问题。NGINX 的默认文件上传大小为 1MB，您需要在 Nginx sites-available 文件中添加以下内容。</font></font><code>client_max_body_size SIZE;</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">其中“SIZE”为 50M，例如 50MB 的文件。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">问题 3：为什么我的下载超时</font></font></h3><a id="user-content-q3-why-is-my-download-timing-out" class="anchor" aria-label="永久链接：问题 3：为什么我的下载超时了" href="#q3-why-is-my-download-timing-out"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NGINX 默认有超时值，因此如果你在 NGINX 后面运行 Stirling-PDF，则可能需要设置超时值，例如添加配置</font></font><code>proxy_read_timeout 3600;</code></p>
</article></div>
